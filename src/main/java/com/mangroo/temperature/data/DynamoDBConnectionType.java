package com.mangroo.temperature.data;

public enum DynamoDBConnectionType {

    LOCAL,
    REMOTE_KEYS,
    REMOTE_INSTANCE_PROFILE
}
